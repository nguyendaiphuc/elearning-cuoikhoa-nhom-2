import CircleNotificationsIcon from "@mui/icons-material/CircleNotifications";
import EmailIcon from "@mui/icons-material/Email";
import ViewWeekIcon from "@mui/icons-material/ViewWeek";
// import useSelection from "antd/lib/table/hooks/useSelection";
import React, { useCallback } from "react";
import {
  Button,
  Dropdown,
  DropdownButton,
  Figure,
  Nav,
  Navbar,
} from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { actLogOut } from "../../../store/actions/auth";
import { NavLink } from "react-router-dom";

Header.propTypes = {};

function Header({ toggleSideMenu }) {
  const renderAvatar = () => (
    <Figure className="m-0">
      <Figure.Image
        width={50}
        height={50}
        alt="avatar-admin"
        src="/img/user.png"
        className="mb-0"
      />
    </Figure>
  );
  const me = useSelector((state) => state.me);
  const dispatch = useDispatch();
  const handleLogOut = useCallback(() => {
    dispatch(actLogOut());

    window.location.replace("/");
  }, [dispatch]);
  return (
    <>
      <Navbar
        collapseOnSelect
        expand="lg"
        bg="dark"
        variant="dark"
        className="fixed-top sb-topnav navbar navbar-expand px-2 py-0"
      >
        {/* <div className="d-flex justify-content-between"> */}

        <Navbar.Brand href="#home" className="me-md-5 pe-sm-4">
          E-Learning
        </Navbar.Brand>
        <Button
          variant="outline-secondary"
          className="btn-sm ms-md-5 ms-0"
          onClick={toggleSideMenu}
        >
          <ViewWeekIcon />
        </Button>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse
          id="responsive-navbar-nav"
          className="align-items-center justify-content-end"
        >
          <Nav>
            <DropdownButton variant="dark" title={<CircleNotificationsIcon />}>
              <Dropdown.ItemText>Nulla vitae elit libero</Dropdown.ItemText>
              <Dropdown.Item as="button">Action</Dropdown.Item>
              <Dropdown.Item as="button">Another action</Dropdown.Item>
              <Dropdown.Item as="button">Something else</Dropdown.Item>
            </DropdownButton>
            <DropdownButton
              variant="dark"
              title={<EmailIcon />}
              className="mx-1"
            >
              <Dropdown.ItemText>Nulla vitae elit libero</Dropdown.ItemText>
              <Dropdown.Item as="button">Action</Dropdown.Item>
              <Dropdown.Item as="button">Another action</Dropdown.Item>
              <Dropdown.Item as="button">Something else</Dropdown.Item>
            </DropdownButton>
            <DropdownButton variant="dark" title={renderAvatar()}>
              <Dropdown.ItemText>
                <Figure className="d-flex align-items-center">
                  <Figure.Image
                    width={50}
                    height={50}
                    alt="avatar-admin"
                    src="/img/user.png"
                  />
                  <Figure.Caption className="ms-2">{me.hoTen}</Figure.Caption>
                </Figure>
              </Dropdown.ItemText>
              {/* <Dropdown.Item as="button">Action</Dropdown.Item> */}
              <Dropdown.Item as="button">
                <NavLink to="/profile" className="header-drop-down-icon">
                  <i className="fas fa-user"></i>
                  <span className="p-30">Profile</span>
                </NavLink>
              </Dropdown.Item>
              <Dropdown.Item as="button" onClick={handleLogOut}>
                <i className="fas fa-sign-out-alt"></i>
                <span className="p-30">Logout</span>
              </Dropdown.Item>
            </DropdownButton>
          </Nav>
        </Navbar.Collapse>
        {/* </div> */}
      </Navbar>
    </>
  );
}

export default Header;
