# Elearning Project
### Team size: 2
- https://gitlab.com/nguyendaiphuc
- https://gitlab.com/nguyentri2815
#### Work Assignment
- https://github.com/nguyendaiphuc  
 *Home Page*
- https://gitlab.com/nguyentri2815   
 *Admin Page*
### Features: 
- Login, logout, register.
- Fully responsive.
- Admin Features: CRUD locations, course, users.
### Technologies:
- ReactJS
- Sass
- Material UI, Formik, yup, react router dom, redux, redux thunk, axios,..
### Live Demo:
https://bc09-elearning-pi.vercel.app/
